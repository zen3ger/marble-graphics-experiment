use crate::render::{Renderer, Viewport};
use glium::glutin::event_loop::{ControlFlow, EventLoop};
use glium::glutin::{self, event, window};

pub struct Application {
    event_loop: EventLoop<()>,
    window: window::WindowBuilder,
    context: glutin::ContextBuilder<'static, glutin::NotCurrent>,
    viewport: Viewport,
}

#[derive(Default)]
pub struct ApplicationBuilder {
    dimensions: Option<(f32, f32)>,
    title: Option<String>,
}

impl ApplicationBuilder {
    pub fn build<'a>(self) -> Application {
        let dimension = self.dimensions.unwrap_or((800.0, 600.0));
        let title = self.title.unwrap_or("Marble Application".into());

        let event_loop = EventLoop::new();

        let window = window::WindowBuilder::new()
            .with_inner_size(glutin::dpi::PhysicalSize::new(dimension.0, dimension.1))
            .with_title(title);

        let context = glutin::ContextBuilder::new()
            .with_depth_buffer(24)
            .with_double_buffer(Some(true))
            .with_hardware_acceleration(Some(true));

        let viewport = Viewport::new(dimension.0, dimension.1);

        Application {
            event_loop,
            window,
            context,
            viewport,
        }
    }

    pub fn with_window_size(mut self, w: usize, h: usize) -> Self {
        self.dimensions = Some((w as f32, h as f32));
        self
    }

    pub fn with_title<T: Into<String>>(mut self, title: T) -> Self {
        self.title = Some(title.into());
        self
    }
}

impl Application {
    pub fn new() -> ApplicationBuilder {
        ApplicationBuilder::default()
    }

    pub fn viewport(&self) -> crate::layout::Aabb {
        self.viewport.bounds()
    }

    pub fn run<F>(self, mut f: F)
    where
        F: 'static + FnMut(&Viewport, &mut Renderer),
    {
        let mut dirty = true;

        let event_loop = self.event_loop;
        let window = self.window;
        let context = self.context;
        let mut viewport = self.viewport;

        let display = glium::Display::new(window, context, &event_loop).unwrap();
        let mut renderer = Renderer::new(display);

        event_loop.run(move |event, _, control_flow| {
            *control_flow = match event {
                event::Event::WindowEvent {
                    event: event::WindowEvent::MouseInput { state, .. },
                    ..
                } => {
                    dirty = state == event::ElementState::Pressed;
                    ControlFlow::Wait
                }
                event::Event::WindowEvent {
                    event: event::WindowEvent::CloseRequested,
                    ..
                } => ControlFlow::Exit,

                event::Event::WindowEvent {
                    event: event::WindowEvent::Resized(size),
                    ..
                } => {
                    viewport.resize(size.width as f32, size.height as f32);

                    dirty = true;
                    ControlFlow::Wait
                }

                _ => ControlFlow::Wait,
            };

            if dirty {
                renderer.begin(&mut viewport);
                f(&viewport, &mut renderer);
                renderer.end();

                dirty = false;
            }
        });
    }
}

impl Default for Application {
    fn default() -> Self {
        ApplicationBuilder::default().build()
    }
}

use crate::layout::{self, Aabb, Anchor, Angle, Constrain, Point, Scale, Size, Transform, Vector};
use crate::render::Vertex;

use lyon::tessellation::{self, BuffersBuilder};
use lyon::tessellation::{FillOptions, FillTessellator};
use lyon::tessellation::{FillVertex, FillVertexConstructor};
use lyon::tessellation::{StrokeOptions, StrokeTessellator};
use lyon::tessellation::{StrokeVertex, StrokeVertexConstructor};

pub use lyon::tessellation::LineCap;
pub use lyon::tessellation::LineJoin;

type VertexBuffers = tessellation::VertexBuffers<Vertex, u16>;

pub trait GeometryBuilder {
    fn aabb(&self) -> &Aabb;

    fn aabb_mut(&mut self) -> &mut Aabb;

    fn fill(&self, ctor: VertexConstructor, opts: &FillOptions, buf: &mut VertexBuffers);

    fn stroke(&self, ctor: VertexConstructor, opts: &StrokeOptions, buf: &mut VertexBuffers);

    fn center(&self) -> Point {
        self.aabb().center()
    }

    fn rotate(&mut self, theta: Angle) {
        //self.aabb_mut().content_rotated(theta);
    }

    fn scale(&mut self, scale: Scale) {
        //self.aabb_mut().content_scaled(scale);
    }
}

pub struct Geometry {
    buf: VertexBuffers,
    builder: Box<dyn GeometryBuilder>,
}

impl Geometry {
    fn new<B>(builder: B) -> Self
    where
        B: 'static + GeometryBuilder,
    {
        Self {
            buf: VertexBuffers::new(),
            builder: Box::new(builder) as Box<dyn GeometryBuilder>,
        }
    }

    pub fn indices(&self) -> &[u16] {
        self.buf.indices.as_slice()
    }

    pub fn vertices(&self) -> &[Vertex] {
        self.buf.vertices.as_slice()
    }

    pub(crate) fn indices_mut(&mut self) -> &mut [u16] {
        self.buf.indices.as_mut_slice()
    }

    pub(crate) fn vertices_mut(&mut self) -> &mut [Vertex] {
        self.buf.vertices.as_mut_slice()
    }

    pub fn build(&mut self, style: &Style, constrains: Option<(&Aabb, &Constrain)>) {
        let transf = constrains.map(|(aabb, cons)| cons.apply(aabb, self.builder.aabb()));
        self.rebuild(style, transf);
    }

    fn rebuild(&mut self, style: &Style, transf: Option<Transform>) {
        self.buf.indices.clear();
        self.buf.vertices.clear();

        match style.fill {
            FillStyle::Color(ref color) => {
                let ctor = VertexConstructor {
                    id: 0,
                    color: color.clone(),
                    transf: transf.unwrap_or_else(Transform::identity),
                };
                let opts =
                    FillOptions::tolerance(0.1).with_fill_rule(lyon::path::FillRule::EvenOdd);

                self.builder.fill(ctor, &opts, &mut self.buf);
            }
            FillStyle::None => {}
        }

        match style.stroke {
            StrokeStyle::Line {
                ref color,
                line_width,
                start_cap,
                end_cap,
                line_join,
            } => {
                let ctor = VertexConstructor {
                    id: 0,
                    color: color.clone(),
                    transf: transf.unwrap_or_else(Transform::identity),
                };
                let opts = StrokeOptions::default()
                    .with_line_width(line_width)
                    .with_start_cap(start_cap)
                    .with_end_cap(end_cap)
                    .with_line_join(line_join);

                self.builder.stroke(ctor, &opts, &mut self.buf)
            }
            StrokeStyle::None => {
                assert!(
                    style.fill != FillStyle::None,
                    "attempted to draw empty geometry"
                );
            }
        }
    }

    //pub fn aabb(&self) -> &Aabb {
    //    self.builder.aabb()
    //}
}

// FIXME: temporary, just to not forget about it
// This is going to be the entity identifier, maybe?
type Id = u32;

pub struct VertexConstructor {
    id: Id,
    color: Color,
    // u,v coords
    // texture id
    transf: Transform,
}

impl<'t> FillVertexConstructor<Vertex> for VertexConstructor {
    fn new_vertex(&mut self, vertex: FillVertex) -> Vertex {
        let pos = self.transf.transform_point(vertex.position());

        Vertex {
            position: pos.to_array(),
            color: self.color.as_array(),
            id: self.id as u32,
            texture_id: 0,
        }
    }
}

impl StrokeVertexConstructor<Vertex> for VertexConstructor {
    fn new_vertex(&mut self, vertex: StrokeVertex) -> Vertex {
        let pos = self.transf.transform_point(vertex.position());
        Vertex {
            position: pos.to_array(),
            color: self.color.as_array(),
            id: self.id as u32,
            texture_id: 0,
        }
    }
}

struct Rect {
    width: f32,
    height: f32,
    aabb: Aabb,
}

impl Rect {
    fn new(width: f32, height: f32) -> Self {
        Self {
            width,
            height,
            aabb: Aabb::new(Size::new(width, height)),
        }
    }
}

impl GeometryBuilder for Rect {
    fn aabb(&self) -> &Aabb {
        &self.aabb
    }

    fn aabb_mut(&mut self) -> &mut Aabb {
        &mut self.aabb
    }

    fn fill(&self, ctor: VertexConstructor, opts: &FillOptions, buf: &mut VertexBuffers) {
        let mut tess = FillTessellator::new();
        tess.tessellate_rectangle(
            &lyon::geom::rect(0.0, 0.0, self.width, self.height),
            opts,
            &mut BuffersBuilder::new(buf, ctor),
        )
        .expect("failed to fill geometry");
    }

    fn stroke(&self, ctor: VertexConstructor, opts: &StrokeOptions, buf: &mut VertexBuffers) {
        let mut tess = StrokeTessellator::new();
        tess.tessellate_rectangle(
            &lyon::geom::rect(0.0, 0.0, self.width, self.height),
            &opts,
            &mut BuffersBuilder::new(buf, ctor),
        )
        .expect("failed to outline geometry");
    }
}

pub fn rect(width: f32, height: f32) -> Geometry {
    Geometry::new(Rect::new(width, height))
}

pub struct Circle {
    radius: f32,
    aabb: Aabb,
}

impl Circle {
    fn new(radius: f32) -> Self {
        Self {
            radius,
            aabb: Aabb::new(Size::new(2.0 * radius, 2.0 * radius)),
        }
    }
}

impl GeometryBuilder for Circle {
    fn aabb(&self) -> &Aabb {
        &self.aabb
    }

    fn aabb_mut(&mut self) -> &mut Aabb {
        &mut self.aabb
    }

    fn fill(&self, ctor: VertexConstructor, opts: &FillOptions, buf: &mut VertexBuffers) {
        let mut tess = FillTessellator::new();
        tess.tessellate_circle(
            self.aabb.center(),
            self.radius,
            opts,
            &mut BuffersBuilder::new(buf, ctor),
        )
        .expect("failed to fill geometry");
    }

    fn stroke(&self, ctor: VertexConstructor, opts: &StrokeOptions, buf: &mut VertexBuffers) {
        let mut tess = StrokeTessellator::new();
        tess.tessellate_circle(
            self.aabb.center(),
            self.radius,
            &opts,
            &mut BuffersBuilder::new(buf, ctor),
        )
        .expect("failed to outline geometry");
    }
}

pub fn circle(radius: f32) -> Geometry {
    Geometry::new(Circle::new(radius))
}

#[derive(PartialEq)]
pub struct Style {
    fill: FillStyle,
    stroke: StrokeStyle,
}

impl Style {
    pub fn fill_color(color: Color) -> Self {
        Self::fill(FillStyle::Color(color))
    }

    pub fn fill(fill: FillStyle) -> Self {
        Self::new(fill, StrokeStyle::None)
    }

    pub fn stroke(stroke: StrokeStyle) -> Self {
        Self::new(FillStyle::None, stroke)
    }

    pub fn new(fill: FillStyle, stroke: StrokeStyle) -> Self {
        Self { fill, stroke }
    }
}

#[derive(PartialEq)]
pub enum FillStyle {
    Color(Color),
    // LinearGradient
    // RadialGradient
    // Image
    None,
}

#[derive(PartialEq)]
pub enum StrokeStyle {
    Line {
        color: Color,
        line_width: f32,
        start_cap: LineCap,
        end_cap: LineCap,
        line_join: LineJoin,
    },
    // Dotted
    // Dashed
    // Custom
    // Image
    None,
}

impl StrokeStyle {
    pub fn line(color: Color, line_width: f32) -> Self {
        Self::styled_line(
            color,
            line_width,
            LineCap::Butt,
            LineCap::Butt,
            LineJoin::Miter,
        )
    }

    pub fn rounded_line(color: Color, line_width: f32) -> Self {
        Self::styled_line(
            color,
            line_width,
            LineCap::Round,
            LineCap::Round,
            LineJoin::Round,
        )
    }

    pub fn styled_line(
        color: Color,
        line_width: f32,
        start_cap: LineCap,
        end_cap: LineCap,
        line_join: LineJoin,
    ) -> Self {
        Self::Line {
            color,
            line_width,
            start_cap,
            end_cap,
            line_join,
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct Color([f32; 4]);

impl Color {
    pub const WHITE: Color = Color([1.0, 1.0, 1.0, 1.0]);
    pub const BLACK: Color = Color([0.0, 0.0, 0.0, 1.0]);
    pub const RED: Color = Color([1.0, 0.0, 0.0, 1.0]);
    pub const GREEN: Color = Color([0.0, 1.0, 0.0, 1.0]);
    pub const BLUE: Color = Color([0.0, 0.0, 1.0, 1.0]);
    pub const CYAN: Color = Color([0.0, 1.0, 1.0, 1.0]);
    pub const YELLOW: Color = Color([1.0, 1.0, 0.0, 1.0]);
    pub const MAGENTA: Color = Color([1.0, 0.0, 1.0, 1.0]);
    pub const ALPHA: Color = Color([0.0, 0.0, 0.0, 0.0]);

    pub fn rgb(r: f32, g: f32, b: f32) -> Self {
        Self::rgba(r, g, b, 1.0)
    }

    pub fn rgba(r: f32, g: f32, b: f32, a: f32) -> Self {
        Self([r, g, b, a])
    }

    pub fn r(&self) -> f32 {
        self.0[0]
    }
    pub fn g(&self) -> f32 {
        self.0[1]
    }
    pub fn b(&self) -> f32 {
        self.0[2]
    }
    pub fn a(&self) -> f32 {
        self.0[3]
    }

    pub fn as_array(&self) -> [f32; 4] {
        self.0
    }
}

use lyon::geom::euclid::{self, UnknownUnit};

pub type Angle = euclid::Angle<f32>;
pub type Transform = euclid::Transform2D<f32, UnknownUnit, UnknownUnit>;
pub type Vector = euclid::Vector2D<f32, UnknownUnit>;
pub type Point = euclid::Point2D<f32, UnknownUnit>;
pub type Size = euclid::Size2D<f32, UnknownUnit>;

#[derive(Debug, Clone)]
pub struct Constrain {
    pad: Padding,
    align: Alignment,
    fill: Fill,
    rotate: Rotation,
}

#[derive(Debug, Eq, PartialEq, Clone, Copy)]
pub enum Alignment {
    /// The geometry is placed at the origin of the constrained area.
    None,
    /// The geometry is centered in the constrained area horizontaly and verticaly.
    Center,
    /// The geometry is centered verticaly in the constrained area.
    Vertical,
    /// The geometry is centered horizontaly in the constrained area.
    Horizontal,
}

#[derive(Debug, Eq, PartialEq, Clone, Copy)]
pub enum Fill {
    /// The geometry is kept as is.
    None,
    /// The geometry is scaled along the x-axis and y-axis to fill the available space.
    Available,
    /// Like `Fill::Available`, but keeps the aspect ratio of the geometry.
    BestFit,
    /// The geometry is scaled along the x-axis to fill the available horizontal space.
    Horizontal,
    /// The geometry is scaled along the y-axis to fill the available vertical space.
    Vertical,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Rotation {
    pub anchor: Anchor,
    pub angle: Angle,
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Padding {
    pub top: f32,
    pub right: f32,
    pub bottom: f32,
    pub left: f32,
}

#[derive(Debug, Copy, Clone)]
pub struct Scale {
    pub x: f32,
    pub y: f32,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Anchor {
    Origin,
    Center,
    Point(f32, f32),
}

impl Constrain {
    pub fn new() -> Self {
        Self {
            pad: Padding::default(),
            align: Alignment::None,
            fill: Fill::None,
            rotate: Rotation {
                anchor: Anchor::Center,
                angle: Angle::radians(0.0),
            },
        }
    }

    #[inline]
    pub fn with_padding(self, pad: Padding) -> Self {
        self.with_top_padding(pad.top)
            .with_right_padding(pad.right)
            .with_bottom_padding(pad.bottom)
            .with_left_padding(pad.left)
    }

    #[inline]
    pub fn with_top_padding(mut self, pad: f32) -> Self {
        self.pad.top = pad;
        self
    }

    #[inline]
    pub fn with_right_padding(mut self, pad: f32) -> Self {
        self.pad.right = pad;
        self
    }

    #[inline]
    pub fn with_bottom_padding(mut self, pad: f32) -> Self {
        self.pad.bottom = pad;
        self
    }

    #[inline]
    pub fn with_left_padding(mut self, pad: f32) -> Self {
        self.pad.left = pad;
        self
    }

    #[inline]
    pub fn with_fill(mut self, fill: Fill) -> Self {
        self.fill = fill;
        self
    }

    #[inline]
    pub fn with_alignment(mut self, align: Alignment) -> Self {
        self.align = align;
        self
    }

    #[inline]
    pub fn with_rotation(mut self, rotate: Rotation) -> Self {
        self.rotate = rotate;
        self
    }

    pub fn apply(&self, area: &Aabb, target: &Aabb) -> Transform {
        let avail = area.inner(&self.pad);
        let anchors = anchors(self.align, &avail, target);
        let mut transform = Transform::identity();

        match self.fill {
            Fill::None => {}
            Fill::Available => {
                let x = avail.width() / target.width();
                let y = avail.height() / target.height();
                transform = apply_at(transform, anchors.fill, |t| t.then_scale(x, y));
            }
            Fill::BestFit => {
                let tw = target.width();
                let th = target.height();
                let x = avail.width() / tw;
                let y = avail.height() / th;
                let scale = if tw > th { x } else { y };
                transform = apply_at(transform, anchors.fill, |t| t.then_scale(scale, scale));
            }
            Fill::Horizontal => {
                let scale = avail.width() / target.width();
                transform = apply_at(transform, anchors.fill, |t| t.then_scale(scale, 1.0));
            }
            Fill::Vertical => {
                let scale = avail.height() / target.height();
                transform = apply_at(transform, anchors.fill, |t| t.then_scale(1.0, scale));
            }
        }

        if self.rotate.angle.radians != 0.0 {
            transform = match self.rotate.anchor {
                Anchor::Origin => transform.then_rotate(self.rotate.angle),
                Anchor::Center => apply_at(transform, target.center().to_vector(), |t| {
                    t.then_rotate(self.rotate.angle)
                }),
                Anchor::Point(x, y) => apply_at(transform, Vector::new(x, y), |t| {
                    t.then_rotate(self.rotate.angle)
                }),
            };
        }

        let align = match self.align {
            Alignment::None => {
                let tx = target.left();
                let ty = target.bottom();
                anchors.align - Vector::new(tx, ty)
            }
            Alignment::Center => anchors.align - target.center().to_vector(),
            Alignment::Horizontal => {
                let tx = target.center_x();
                let ty = target.bottom();
                anchors.align - Vector::new(tx, ty)
            }
            Alignment::Vertical => {
                let tx = target.left();
                let ty = target.center_y();
                anchors.align - Vector::new(tx, ty)
            }
        };

        transform.then_translate(align)
    }
}

#[derive(Debug, Clone, Default)]
pub struct Aabb {
    init: bool,
    p0: Point,
    p1: Point,
}

impl Aabb {
    pub fn new(size: Size) -> Self {
        Self {
            init: true,
            p0: Point::origin(),
            p1: Point::new(size.width, size.height),
        }
    }

    pub fn inner(&self, pad: &Padding) -> Self {
        let x0 = (self.p0.x + pad.left).min(self.p1.x - pad.right);
        let x1 = (self.p0.x + pad.left).max(self.p1.x - pad.right);
        let y0 = (self.p0.y + pad.bottom).min(self.p1.y - pad.top);
        let y1 = (self.p0.y + pad.bottom).max(self.p1.y - pad.top);

        Aabb {
            init: true,
            p0: Point::new(x0, y0),
            p1: Point::new(x1, y1),
        }
    }

    pub fn shrink(self, pad: &Padding) -> Self {
        self.inner(pad)
    }

    pub fn contains(&self, x: f32, y: f32) -> bool {
        (self.p0.x..self.p1.x).contains(&x) && (self.p0.y..self.p1.y).contains(&y)
    }

    pub fn overlaps(&self, other: &Self) -> bool {
        self.contains(other.p0.x, other.p0.y)
            || self.contains(other.p0.x, other.p1.y)
            || self.contains(other.p1.x, other.p0.y)
            || self.contains(other.p1.x, other.p1.y)
            || other.contains(self.p0.x, self.p0.y)
            || other.contains(self.p0.x, self.p1.y)
            || other.contains(self.p1.x, self.p0.y)
            || other.contains(self.p1.x, self.p1.y)
    }

    #[inline]
    pub fn width(&self) -> f32 {
        self.p1.x - self.p0.x
    }

    #[inline]
    pub fn height(&self) -> f32 {
        self.p1.y - self.p0.y
    }

    #[inline]
    pub fn top(&self) -> f32 {
        self.p1.y
    }

    #[inline]
    pub fn right(&self) -> f32 {
        self.p1.x
    }

    #[inline]
    pub fn bottom(&self) -> f32 {
        self.p0.y
    }

    #[inline]
    pub fn left(&self) -> f32 {
        self.p0.x
    }

    #[inline]
    pub fn center(&self) -> Point {
        Point::new(self.center_x(), self.center_y())
    }

    #[inline]
    pub fn center_x(&self) -> f32 {
        self.p0.x + self.width() * 0.5
    }

    #[inline]
    pub fn center_y(&self) -> f32 {
        self.p0.y + self.height() * 0.5
    }
}

struct Anchors {
    align: Vector,
    fill: Vector,
}

// Get the anchors points needed for scaling and aligning the geometry surrounded by `to_fit`
// into area noted by `is_avail`.
fn anchors(align: Alignment, is_avail: &Aabb, to_fit: &Aabb) -> Anchors {
    match align {
        Alignment::None => Anchors {
            align: Vector::new(is_avail.left(), is_avail.bottom()),
            fill: Vector::new(to_fit.left(), to_fit.bottom()),
        },
        Alignment::Center => Anchors {
            align: Vector::new(is_avail.center_x(), is_avail.center_y()),
            fill: Vector::new(to_fit.center_x(), to_fit.center_y()),
        },
        Alignment::Horizontal => Anchors {
            align: Vector::new(is_avail.center_x(), is_avail.bottom()),
            fill: Vector::new(to_fit.center_x(), to_fit.bottom()),
        },
        Alignment::Vertical => Anchors {
            align: Vector::new(is_avail.left(), is_avail.center_y()),
            fill: Vector::new(to_fit.left(), to_fit.center_y()),
        },
    }
}

#[inline]
pub(crate) fn apply_at(
    transform: Transform,
    anchor: Vector,
    f: impl FnOnce(Transform) -> Transform,
) -> Transform {
    f(transform.then_translate(anchor.component_mul(Vector::splat(-1.0)))).then_translate(anchor)
}

#[allow(dead_code)]
mod ui;

mod app;
mod draw;
mod layout;
mod render;

use draw::*;
use layout::*;

fn main() {
    let app = app::Application::new()
        .with_title("Marble")
        .with_window_size(500, 500)
        .build();

    let viewport = app.viewport();
    let constrain = Constrain::new()
        .with_padding(Padding {
            top: 10.0,
            right: 100.0,
            bottom: 10.0,
            left: 100.0,
        })
        .with_alignment(Alignment::Center)
        .with_fill(Fill::Available);

    let mut area = draw::rect(viewport.width(), viewport.height());
    let area_style = Style::stroke(StrokeStyle::line(Color::GREEN, 1.0));

    let mut rect = draw::rect(100.0, 10.0);
    let rect_style = Style::fill_color(Color::WHITE);

    let mut angle = 0.0;
    app.run(move |viewport, renderer| {
        let start = std::time::Instant::now();

        let viewport = viewport.bounds();

        let rotated = Constrain::new()
            .with_padding(Padding {
                top: 20.0,
                right: 200.0,
                bottom: 20.0,
                left: 200.0,
            })
            .with_alignment(Alignment::Vertical)
            .with_fill(Fill::BestFit)
            .with_rotation(Rotation {
                anchor: Anchor::Point(0.0, 5.0),
                angle: Angle::radians(angle),
            });

        rect.build(&rect_style, Some((&viewport, &rotated)));
        area.build(&area_style, Some((&viewport, &constrain)));

        renderer.paint(&mut rect);
        renderer.paint(&mut area);

        let duration = start.elapsed();
        println!("RENDER TIME: {} [s]", duration.as_secs_f32());

        angle += std::f32::consts::FRAC_PI_8;
    });
}

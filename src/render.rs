use glium::{self, Surface};
use lyon::geom::euclid;

use crate::draw::Geometry;

#[derive(Copy, Clone, Debug)]
pub struct Vertex {
    pub position: [f32; 2],
    pub color: [f32; 4],

    pub id: u32,
    // TODO: tiling factor for the texture
    // TODO: (U,V) coordinates for the texture
    pub texture_id: i32,
}

glium::implement_vertex!(Vertex, position, color, id, texture_id);

const VERTEX_SHADER: &'static str = include_str!("shader.vert");
const FRAGMENT_SHADER: &'static str = include_str!("shader.frag");

const VERTICES_CAP: usize = 10000;
const INDICES_CAP: usize = VERTICES_CAP + VERTICES_CAP / 2;

pub struct Renderer {
    vertices: glium::VertexBuffer<Vertex>,
    vertices_len: usize,

    indices: glium::IndexBuffer<u16>,
    indices_len: usize,

    display: glium::Display,
    frame: Option<glium::Frame>,
    projection: [[f32; 4]; 4],

    program: glium::Program,
}

impl Renderer {
    pub fn new(display: glium::Display) -> Self {
        let vertices = glium::VertexBuffer::empty_dynamic(&display, VERTICES_CAP).unwrap();
        let indices = glium::IndexBuffer::empty_dynamic(
            &display,
            glium::index::PrimitiveType::TrianglesList,
            INDICES_CAP,
        )
        .unwrap();
        let program =
            glium::Program::from_source(&display, VERTEX_SHADER, FRAGMENT_SHADER, None).unwrap();

        Self {
            vertices,
            vertices_len: 0,
            indices,
            indices_len: 0,
            display,
            frame: None,
            program,
            projection: [[0.0; 4]; 4],
        }
    }

    #[inline]
    fn would_overflow(&self, geom: &Geometry) -> bool {
        let is = geom.indices().len() + self.vertices_len;
        let vs = geom.vertices().len() + self.indices_len;

        vs >= VERTICES_CAP || is >= INDICES_CAP
    }

    pub fn paint(&mut self, geom: &mut Geometry) {
        if self.would_overflow(geom) {
            self.flush()
        }

        // Send vertex data to GPU.
        let offset = {
            let vs = geom.vertices_mut();
            let begin = self.vertices_len;
            let end = begin + vs.len();
            self.vertices.slice(begin..end).unwrap().write(&vs);
            self.vertices_len += vs.len();

            begin as u16
        };

        // Send index data to GPU.
        {
            let is = geom.indices_mut();
            let begin = self.indices_len;
            let end = begin + is.len();
            // Scan the first triangle for smallest index!
            // This is needed as previous rendering might have had the geometry
            // rendered to a different buffer position.
            let align = *is[0..2].into_iter().min().unwrap_or(&0);
            if align == offset {
                // Lucky! We start rendering at the same buffer position,
                // so we can skip the alignment of the indices!
            } else {
                for i in is.iter_mut() {
                    *i = *i - align + (offset as u16);
                }
            }
            self.indices.slice(begin..end).unwrap().write(&is);
            self.indices_len += is.len();
        }
    }

    #[inline]
    pub fn begin(&mut self, viewport: &mut Viewport) {
        self.frame = {
            let mut frame = self.display.draw();
            frame.clear_color_and_depth((0.0, 0.0, 0.0, 1.0), 1.0);
            Some(frame)
        };

        if viewport.did_change() {
            self.projection = viewport.projection();
        }
    }

    #[inline]
    pub fn end(&mut self) {
        self.flush();

        let frame = self.frame.take().unwrap();
        frame.finish().unwrap();
    }

    fn flush(&mut self) {
        if self.vertices_len == 0 {
            return;
        }

        let frame = self.frame.as_mut().unwrap();

        let uniforms = glium::uniform!(view_projection: self.projection);
        let draw_params = glium::draw_parameters::DrawParameters {
            blend: glium::Blend::alpha_blending(),
            ..Default::default()
        };

        frame
            .draw(
                &self.vertices,
                // NOTE: this is needed to communicate to glDrawElements how
                // many indicies are *actually* used
                &self.indices.slice(0..self.indices_len).unwrap(),
                &self.program,
                &uniforms,
                &draw_params,
            )
            .unwrap();

        self.vertices_len = 0;
        self.vertices.invalidate();
        self.indices_len = 0;
        self.indices.invalidate();
    }
}

pub struct Viewport {
    projection: euclid::Transform3D<f32, euclid::UnknownUnit, euclid::UnknownUnit>,
    width: f32,
    height: f32,
    dirty: bool,
}

impl Viewport {
    pub fn new(width: f32, height: f32) -> Self {
        let projection = euclid::Transform3D::ortho(0.0, width, 0.0, height, -1.0, 1.0);

        Self {
            projection,
            width,
            height,
            dirty: true,
        }
    }

    fn did_change(&self) -> bool {
        self.dirty
    }

    pub fn resize(&mut self, width: f32, height: f32) {
        if self.width == width && self.height == height {
            return;
        }

        let projection = euclid::Transform3D::ortho(0.0, width, 0.0, height, -1.0, 1.0);
        self.projection = projection;
        self.width = width;
        self.height = height;
        self.dirty = true;
    }

    pub fn projection(&mut self) -> [[f32; 4]; 4] {
        self.dirty = false;
        self.projection.to_arrays()
    }

    pub fn bounds(&self) -> crate::layout::Aabb {
        use crate::layout;
        layout::Aabb::new(layout::Size::new(self.width, self.height))
    }
}

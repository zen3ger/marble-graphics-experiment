#version 450 core

struct OutputVertex {
    vec4 color;
    uint id;
    int texture_id;
};

layout(location = 0) out vec4 color;

layout(location = 0) in flat OutputVertex input_vertex;

// TODO: array of Sampler2D to select texture from!

void main() {
    color = input_vertex.color;
}

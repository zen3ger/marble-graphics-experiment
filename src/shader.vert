#version 450 core

layout(location = 0) in vec2 position;
layout(location = 1) in vec4 color;
layout(location = 2) in uint id;
layout(location = 3) in int texture_id;

struct OutputVertex {
    vec4 color;
    uint id;
    int texture_id;
};

layout(location = 0) out OutputVertex out_vertex;

uniform mat4 view_projection;

void main() {
    out_vertex.color = color;
    out_vertex.id = id;
    out_vertex.texture_id = texture_id;

    vec3 pos = vec3(position, 0.0);
    gl_Position = view_projection * vec4(pos, 1.0);
}

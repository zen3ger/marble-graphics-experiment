use std::any::TypeId;
use std::collections::{HashMap, HashSet};
use std::iter::Iterator;

pub type Entity = u64;

pub struct Handle<'a> {
    composer: &'a mut Composer,
    entity: Entity,
}

impl<'a> Handle<'a> {
    pub fn set<T: 'static>(&mut self, comp: T) -> Option<T> {
        self.composer.add_storage::<T>();
        self.composer.set(self.entity, comp)
    }

    pub fn get<T: 'static>(&mut self) -> Option<&T> {
        if self.composer.has_storage::<T>() {
            self.composer.get(self.entity)
        } else {
            None
        }
    }

    pub fn get_mut<T: 'static>(&mut self) -> Option<&mut T> {
        if self.composer.has_storage::<T>() {
            self.composer.get_mut(self.entity)
        } else {
            None
        }
    }

    pub fn remove<T: 'static>(&mut self) -> Option<T> {
        if self.composer.has_storage::<T>() {
            self.composer.remove(self.entity)
        } else {
            None
        }
    }

    pub fn destroy(self) {
        self.composer.destroy(self.entity);
    }

    pub fn into(self) -> Entity {
        self.entity
    }
}

pub struct Composer {
    stores: HashMap<TypeId, Box<Storage>>,
    entities: HashSet<Entity>,
    next_entity: Entity,
}

impl Composer {
    pub fn new() -> Self {
        Self {
            stores: HashMap::new(),
            entities: HashSet::new(),
            next_entity: 0,
        }
    }

    fn get_internal<T: 'static>(&self) -> Option<&InternalStorage<T>> {
        self.stores.get(&TypeId::of::<T>()).map(|boxed| unsafe {
            boxed
                .as_ref()
                .0
                .downcast_ref_unchecked::<InternalStorage<T>>()
        })
    }

    fn get_mut_internal<T: 'static>(&mut self) -> Option<&mut InternalStorage<T>> {
        self.stores.get_mut(&TypeId::of::<T>()).map(|boxed| unsafe {
            boxed
                .as_mut()
                .0
                .downcast_mut_unchecked::<InternalStorage<T>>()
        })
    }

    pub fn with_storage<T: 'static>(mut self) -> Self {
        self.add_storage::<T>();
        self
    }

    pub fn add_storage<T: 'static>(&mut self) -> bool {
        let mut new = false;
        self.stores.entry(TypeId::of::<T>()).or_insert_with(|| {
            new = true;
            Storage::new::<T>()
        });

        new
    }

    pub fn remove_storage<T: 'static>(&mut self) -> bool {
        self.stores.remove(&TypeId::of::<T>()).is_some()
    }

    pub fn has_storage<T: 'static>(&self) -> bool {
        self.stores.contains_key(&TypeId::of::<T>())
    }

    pub fn set<T: 'static>(&mut self, entity: Entity, comp: T) -> Option<T> {
        self.get_mut_internal()
            .expect("unregistered storage type")
            .0
            .insert(entity, comp)
    }

    pub fn get<T: 'static>(&self, entity: Entity) -> Option<&T> {
        self.get_internal()
            .expect("unregistered storage type")
            .0
            .get(&entity)
    }

    pub fn get_mut<T: 'static>(&mut self, entity: Entity) -> Option<&mut T> {
        self.get_mut_internal()
            .expect("unregistered storage type")
            .0
            .get_mut(&entity)
    }

    pub fn remove<T: 'static>(&mut self, entity: Entity) -> Option<T> {
        self.get_mut_internal()
            .expect("unregistered storage type")
            .0
            .remove(&entity)
    }

    pub fn entities(&self) -> impl Iterator<Item = &Entity> {
        self.entities.iter()
    }

    pub fn create(&mut self) -> Entity {
        let entity = self.next_entity;
        self.entities.insert(entity);
        self.next_entity += 1;

        entity
    }

    pub fn destroy(&mut self, entity: Entity) -> bool {
        if self.entities.remove(&entity) {
            for store in self.stores.values_mut() {
                store.0.remove(entity);
            }
            true
        } else {
            false
        }
    }

    pub fn get_handle<'a>(&'a mut self, entity: Entity) -> Handle<'a> {
        Handle {
            composer: self,
            entity,
        }
    }

    pub fn create_handle<'a>(&'a mut self) -> Handle<'a> {
        let entity = self.create();
        self.get_handle(entity)
    }
}

struct Storage(dyn Store);
struct StorageWrapper<T: ?Sized>(T);

impl Storage {
    fn new<T>() -> Box<Self> {
        let un_sized =
            Box::new(StorageWrapper(InternalStorage::<T>::new())) as Box<StorageWrapper<dyn Store>>;
        unsafe { std::mem::transmute(un_sized) }
    }
}

struct InternalStorage<T>(HashMap<Entity, T>);

impl<T> InternalStorage<T> {
    fn new() -> Self {
        Self(HashMap::new())
    }
}

trait Store {
    fn remove(&mut self, entity: Entity) -> bool;
}

impl<T> Store for InternalStorage<T> {
    fn remove(&mut self, entity: Entity) -> bool {
        self.0.remove(&entity).is_some()
    }
}

unsafe impl UnsafeAny for dyn Store {}

unsafe fn as_unit_ptr<T: ?Sized>(ptr: *const T) -> *const () {
    ptr as *const ()
}

unsafe fn as_mut_unit_ptr<T: ?Sized>(ptr: *mut T) -> *mut () {
    ptr as *mut ()
}

unsafe trait UnsafeAny {
    unsafe fn downcast_ref_unchecked<T>(&self) -> &T {
        std::mem::transmute(as_unit_ptr(self))
    }

    unsafe fn downcast_mut_unchecked<T>(&mut self) -> &mut T {
        std::mem::transmute(as_mut_unit_ptr(self))
    }
}
